import * as React from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Tooltip,
  CartesianGrid,
} from "recharts";

import { StreamListener, SineStream } from "./SineStream";

export function App() {
  const [data, setData] = React.useState<[number, number][]>([]);

  function addData(newValue: [number, number]) {
    setData((data) => {
      if (data.length > 100) {
        data.shift();
      }
      return [...data, newValue];
    });
  }

  React.useEffect(() => {
    const stream = new SineStream();
    stream.subscribe(addData);
  }, []);

  return (
    <>
      <h1>Welcome</h1>
      <LineChart
        width={800}
        height={400}
        margin={{ top: 5, right: 20, left: 10, bottom: 5 }}
        data={data.map(([v, i]) => ({ name: v, value: i }))}
      >
        <XAxis
          dataKey="name"
          type="number"
          domain={["dataMin", "dataMax"]}
          allowDataOverflow
        />
        <YAxis type="number" yAxisId={0} />
        <Tooltip />
        <CartesianGrid stroke="#f5f5f5" strokeDasharray="3 3" />
        <Line
          type="linear"
          dataKey="value"
          activeDot={false}
          stroke="#ff7300"
          yAxisId={0}
          isAnimationActive={false}
          animationEasing="linear"
          animationDuration={200}
        />
      </LineChart>
    </>
  );
}
