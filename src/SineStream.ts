export type StreamListener = (([x, y]: [number, number]) => void) 

export class SineStream {

    private listener: StreamListener| null = null;
    private counter = 0;

    constructor() {
	setInterval(() => {
	    this.counter++;
	    const value = Math.sin(this.counter/5*Math.PI)
	    if (this.listener) {
		this.listener([this.counter, value + (Math.random()-0.5)]);
	    }
	}, 200)
    }

    subscribe(listener: StreamListener) {
	this.listener = listener;
    }
}
